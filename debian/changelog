python-prctl (1.8.1-2) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Name, Repository-Browse.

 -- Debian Janitor <janitor@jelmer.uk>  Sun, 08 Jan 2023 08:02:14 -0000

python-prctl (1.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Upgrade to debhelper compat level 13.
  * Enable autopkgtest-pkg-python.

 -- Felix Geyer <fgeyer@debian.org>  Sun, 30 Oct 2022 09:17:22 +0100

python-prctl (1.7-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:20:45 -0400

python-prctl (1.7-2) unstable; urgency=medium

  * Team upload.
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Fri, 26 Jul 2019 20:50:43 +0200

python-prctl (1.7-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces

  [ Felix Geyer ]
  * New upstream release.
    - Avoids leaking "x" into the environment. (Closes: #601224)
  * Build-depend on python3-sphinx instead of python-sphinx.

 -- Felix Geyer <fgeyer@debian.org>  Tue, 24 Apr 2018 18:04:15 +0200

python-prctl (1.6.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Felix Geyer <fgeyer@debian.org>  Sun, 18 Jun 2017 09:37:38 +0200

python-prctl (1.6.1-1) experimental; urgency=medium

  * New upstream version (Closes: #750470)
    - Fixes: prctl.set_proctitle corrupts process environment (Closes: #776476)
  * Set maintainer to DPMT with current maintainer's consent
  * Add a python3 package (Closes: #858448)
  * Clean up build-depends
  * Switch to debhelper compat level 10
  * Use Homepage control field for upstream repo
  * Add a watch file
  * Declare compatibility with policy 3.9.8

 -- Felix Geyer <fgeyer@debian.org>  Sat, 06 May 2017 21:20:54 +0200

python-prctl (1.1.1-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Build using dh-python. Closes: #786312.

 -- Matthias Klose <doko@debian.org>  Tue, 18 Aug 2015 18:22:28 +0200

python-prctl (1.1.1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Build documentation in override_dh_auto_build target rather than in build
    target (closes: #666322). Thanks to Lucas Nussbaum for the bug report.
    + Bump build-dependency on debhelper to >= 7.0.50~.

 -- Jakub Wilk <jwilk@debian.org>  Thu, 26 Apr 2012 18:48:35 +0200

python-prctl (1.1.1-1) unstable; urgency=low

  * Allow the running kernel to be < 2.6.26, useful for buildds
  * Initial release to debian proper (Closes: #578328)

 -- Dennis Kaarsemaker <dennis@kaarsemaker.net>  Mon, 19 Apr 2010 21:55:57 +0200

python-prctl (1.1-0~seveas1) lucid; urgency=low

  * New upstream release
   - Add full capability handling

 -- Dennis Kaarsemaker <dennis@kaarsemaker.net>  Sun, 21 Mar 2010 22:36:00 +0100

python-prctl (1.0-0~seveas1) unstable; urgency=low

  * Initial release.

 -- Dennis Kaarsemaker <dennis@kaarsemaker.net>  Fri, 12 Mar 2010 21:39:15 +0100
